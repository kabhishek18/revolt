import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import { GalleryComponent } from './gallery/gallery.component';
import { ServiceComponent } from './service/service.component';
import { TestimonialsComponent } from './testimonials/testimonials.component';
import { ClientsComponent } from './clients/clients.component';
import { PriceComponent } from './price/price.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { NavComponent } from './nav/nav.component';
import { SocialComponent } from './social/social.component';
import { BannerComponent } from './banner/banner.component';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
 { path: '',
    redirectTo: 'index',
    pathMatch: 'full'
  },
		{ path: 'index', component: HomeComponent },
		{ path: 'Banner', component: BannerComponent },
		{ path: 'About', component: AboutComponent },
		{ path: 'Gallery', component: GalleryComponent },
		{ path: 'Services', component: ServiceComponent },
		{ path: 'Testimonials', component: TestimonialsComponent },
		{ path: 'Clients', component: ClientsComponent },
		{ path: 'Pricing', component: PriceComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }








